export const controls = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerTwoBlock2: 'KeyV',
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO']
}

// if (firstFighterSuperAttackAvailable) {
//   useSuperAttackFunc(
//     firstFighter,
//     secondFighter,
//     secondFighterHealth,
//     secondFighterLineHealth,
//     controls.PlayerOneCriticalHitCombination,
//     1,
//     ...controls.PlayerOneCriticalHitCombination)
// }
//
// if (secondFighterSuperAttackAvailable) {
//   useSuperAttackFunc(
//     secondFighter,
//     firstFighter,
//     firstFighterHealth,
//     firstFighterLineHealth,
//     controls.PlayerTwoCriticalHitCombination,
//     2,
//     ...controls.PlayerTwoCriticalHitCombination)
// }