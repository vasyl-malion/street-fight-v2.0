import { controls } from '../../constants/controls';

let firstFighterSuperAttackAvailable = true
let secondFighterSuperAttackAvailable = true

export async function fight(firstFighter, secondFighter) {

  const { health: firstFighterHealth} = firstFighter
  const { health: secondFighterHealth} = secondFighter

  let firstFighterBlockAvailable = false
  let secondFighterBlockAvailable = false

  const firstFighterLineHealth = document.getElementById('left-fighter-indicator')
  const secondFighterLineHealth = document.getElementById('right-fighter-indicator')

  return new Promise((resolve) => {

    if (firstFighterSuperAttackAvailable) {
      useSuperAttackPress( () => useSuperAttack(
        firstFighter,
        secondFighter,
        secondFighterHealth,
        secondFighterLineHealth,
        1),
        ...controls.PlayerOneCriticalHitCombination
      )
    }

    if (secondFighterSuperAttackAvailable) {
      useSuperAttackPress( () => useSuperAttack(
        secondFighter,
        firstFighter,
        firstFighterHealth,
        firstFighterLineHealth,
        2),
        ...controls.PlayerTwoCriticalHitCombination)
    }

    document.addEventListener('keydown', function(e) {
      switch (e.code) {
        case controls.PlayerOneBlock:
          firstFighterBlockAvailable = true
          break;
        case controls.PlayerTwoBlock:
          secondFighterBlockAvailable = true
          break;
      }
    });

    document.addEventListener('keyup', function(e) {
      switch (e.code) {
        case controls.PlayerOneBlock:
          firstFighterBlockAvailable = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighterBlockAvailable = false;
          break;
        case controls.PlayerOneAttack:
          playerAttack(
            firstFighter,
            secondFighter,
            secondFighterHealth,
            secondFighterLineHealth,
            firstFighterBlockAvailable,
            secondFighterBlockAvailable
          )
          break
        case controls.PlayerTwoAttack:
          playerAttack(
            secondFighter,
            firstFighter,
            firstFighterHealth,
            firstFighterLineHealth,
            secondFighterBlockAvailable,
            firstFighterBlockAvailable
          )
          break;
      }

      if (firstFighter.health <= 0 ) {
        resolve(secondFighter)
      } else if (secondFighter.health <= 0) {
        resolve(firstFighter)
      }
    })
  })
}

const useSuperAttack = (attacker, defender, fighterHealth, fighterLineHealth, superAttackAvailable) => {
  if (superAttackAvailable === 1) {
    if (firstFighterSuperAttackAvailable) {
      minusHealth(attacker, defender, fighterHealth, fighterLineHealth)
      firstFighterSuperAttackAvailable = false
      setTimeout(() => firstFighterSuperAttackAvailable = true, 10000)
    }
  } else if (superAttackAvailable === 2) {
    if (secondFighterSuperAttackAvailable) {
      minusHealth(attacker, defender, fighterHealth, fighterLineHealth)
      secondFighterSuperAttackAvailable = false
      setTimeout(() => secondFighterSuperAttackAvailable = true, 10000)
    }
  }
}

const minusHealth = (attacker, defender, fighterHealth, fighterLineHealth) => {
  defender.health -= getSuperPower(attacker)
  getFighterHealth(defender, fighterHealth, fighterLineHealth)
}

const playerAttack = (attacker, defender, fighterHealth,
                      fighterLineHealth, attackerFighterBlockAvailable, defenderFighterBlockAvailable) => {
  if (!attackerFighterBlockAvailable) {

    if(!defenderFighterBlockAvailable) {
      defender.health -= getDamage(attacker, defender)
    }

    getFighterHealth(defender, fighterHealth, fighterLineHealth)
  }
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender)
  damage = damage >= 0 ? damage : 0
  return damage
}

export function getSuperPower(fighter) {
  let power = fighter.attack * 2
  return power
}

export function getHitPower(fighter) {
  const power = fighter.attack * (1 + Math.random())
  return power
}

export function getBlockPower(fighter) {
  const block =  fighter.defense * (1 + Math.random())
  return block
}

function getFighterHealth(fighter, initialFighterHealth, lineHealth) {
  const result  = fighter.health / initialFighterHealth * 100
  if (result > 0) {
    lineHealth.style.width = `${result}%`
  } else {
    lineHealth.style.width = `0%`
  }
  return result;
}

function useSuperAttackPress(callback, ...codes) {
  let pressed = new Set()

  document.addEventListener('keydown', function(event) {
    pressed.add(event.code)

    for (let code of codes) {
      if (!pressed.has(code)) {
        return;
      }
    }
    pressed.clear()

    callback()
  });

  document.addEventListener('keyup', function(event) {
    pressed.delete(event.code)
  });
}